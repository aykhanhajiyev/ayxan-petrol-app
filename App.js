import React, { useState } from "react";
import { StatusBar } from "react-native";
import { Provider } from "react-redux";
import { AppLoading } from "expo";

import { RootNav } from "./navigation";
import { loadFonts } from "./styles/fonts";
import store from "./store";

export default function App() {
  const [loaded, setLoaded] = useState(false);
  if (!loaded) {
    return (
      <AppLoading
        startAsync={loadFonts}
        onError={() => console.log("Something went wrong while app loading")}
        onFinish={() => setLoaded(true)}
      />
    );
  }
  return (
    <Provider store={store}>
      <StatusBar barStyle="dark-content" backgroundColor={"white"} translucent={true} />
      <RootNav />
    </Provider>
  );
}
