const COLORS = {
  main: "#bdc3c7",
  white: "#fff",
  btn: "#34495e",
  header:"#2980b9"
};

export default COLORS;
