import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { TableScreen, BazaScreen, HomeScreen } from "../screens";

import { AntDesign } from "@expo/vector-icons";
const { Navigator, Screen } = createBottomTabNavigator();

export const HomeTabs = () => {
  return (
    <Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size }) => {
          let iconName = "home";
          if (route.name == "TableBottom") {
            iconName = "table";
          }
          if (route.name === "BazaBottom") {
            iconName = "database";
          }
          return <AntDesign name={iconName} size={size} color={color} />;
        },
      })}
    >
      <Screen
        name={"HomeBottom"}
        component={HomeScreen}
        options={{ title: "Əsas" }}
      />
      <Screen
        name={"TableBottom"}
        component={TableScreen}
        options={{ title: "Cədvəl" }}
      />
      <Screen
        name={"BazaBottom"}
        component={BazaScreen}
        options={{ title: "Baza" }}
      />
    </Navigator>
  );
};
