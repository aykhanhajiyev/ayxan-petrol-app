import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { AuthStack } from "./AuthStack";
import { HomeStack } from "./HomeStack";
const { Navigator, Screen } = createDrawerNavigator();
export const RootNav = () => {
  return (
    <NavigationContainer>
      <Navigator>
        <Screen name="Auth" component={AuthStack} options={{title:"Çıxış",swipeEnabled:false}}/>
        <Screen name="Home" component={HomeStack} options={{title:"Əsas Səhifə"}} />
      </Navigator>
    </NavigationContainer>
  );
};
