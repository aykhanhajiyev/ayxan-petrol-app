import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { CreateScreen } from "../screens";
import COLORS from "../styles/colors";
import { MenuIcon } from "../components";
import { HomeTabs } from "./HomeTabs";
const { Navigator, Screen } = createStackNavigator();
export const HomeStack = () => {
  return (
    <Navigator
      screenOptions={() => ({
        headerTitleStyle: {
          fontFamily: "MontserratSemiBold",
          color: COLORS.header,
        },
      })}
    >
      <Screen
        name="Home"
        component={HomeTabs}
        options={({ navigation, route }) => ({
          title: "Ayxan Petrol",
          headerRight: () => (
            <MenuIcon
              name="ios-add-circle-outline"
              size={26}
              onPress={() => navigation.navigate("Create")}
            />
          ),
          headerLeft: () => (
            <MenuIcon
              name={"ios-menu"}
              size={30}
              onPress={() => navigation.toggleDrawer()}
            />
          ),
        })}
      />
      <Screen
        name="Create"
        component={CreateScreen}
        options={{ title: "Əlavə et" }}
      />
    </Navigator>
  );
};
