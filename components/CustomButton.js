import React from "react";
import {
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from "react-native";

import COLORS from "../styles/colors";
import { CustomText } from "./CustomText";
export const CustomButton = ({ title, style, onPress, ...rest }) => {
  return (
    <TouchableOpacity style={[styles.btn, style]} onPress={onPress} {...rest}>
      <CustomText weight="semibold" style={styles.text}>{title}</CustomText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    width: Dimensions.get("window").width - 150,
    backgroundColor: "#e74c3c",
    padding: 10,
    alignItems: "center",
    borderRadius: 100,
    marginVertical: 20,
  },
  text: {
    color: COLORS.white,
    fontSize:16,
  },
});
