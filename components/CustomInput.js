import React from "react";
import { TextInput, StyleSheet } from "react-native";

export const CustomInput = ({ style, ...rest }) => {
  return <TextInput style={[style, styles.textInput]} {...rest} autoCapitalize="none"  />;
};

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    backgroundColor: "#eee",
    marginTop: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    fontSize: 18,
    fontFamily: "MontserratRegular",
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
