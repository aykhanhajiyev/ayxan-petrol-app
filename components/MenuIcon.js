import React from "react";
import { TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import COLORS from "../styles/colors";

export const MenuIcon = ({name,size,color,style, onPress, ...rest}) => {
  return (
    <TouchableOpacity style={[{paddingHorizontal:15,paddingTop:5},style]} onPress={onPress} {...rest}>
      <Ionicons
        name={name}
        size={size}
        color={color || COLORS.header}
      />
    </TouchableOpacity>
  );
};
