import React from "react";
import { Text } from "react-native";

const fontFamilies = {
  regular: "MontserratRegular",
  semibold: "MontserratSemiBold",
  bold: "MontserratBold",
};
export const CustomText = ({ children, style, weight, size, ...rest }) => {
  return (
    <Text
      {...rest}
      style={[
        { fontFamily: fontFamilies[weight] || fontFamilies.regular, fontSize:size },
        style,
      ]}
    >
      {children}
    </Text>
  );
};
