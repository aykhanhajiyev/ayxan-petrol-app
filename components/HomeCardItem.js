import React from "react";
import { View, StyleSheet } from "react-native";
import { CustomText } from "./CustomText";

export const HomeCardItem = ({ header, text }) => {
  return (
    <View style={styles.card}>
      <View style={styles.cardLeft}>
        <CustomText weight="semibold" size={20} style={styles.text}>
          {header}
        </CustomText>
      </View>
      <View style={styles.cardRight}>
        <CustomText size={20} style={styles.text}>
          {text}
        </CustomText>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    width: "100%",
    backgroundColor: "#03A9F4",
    borderRadius: 20,
    padding: 10,
    marginVertical: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  text:{
      color:'#fff'
  }
});
