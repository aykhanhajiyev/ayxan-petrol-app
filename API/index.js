const domain = "http://api.ayxanpetrol.tk/api/";

function fetchAPI(endpoint, method) {
  const options = {
    method,
    headers: { "Content-Type": "application/json" },
  };
  return async (args) => {
    let getArgs = "?";
    if (method === "GET") {
      for (let key in args) {
        getArgs += `${key}=${args[key]}&`;
      }
    } else {
      options.body = JSON.stringify(args);
    }
    const response = await fetch(`${domain}${endpoint}${getArgs}`, options);
    return response.json();
  };
}

export const getFuelItemFetch = fetchAPI("ponpas/null/1", "GET");
export const addFuelFetch = fetchAPI("ponpas", "POST");
export const getFuelsFetch = fetchAPI("ponpas", "GET");
export const deleteFuelFetch = async (id) => fetchAPI(`ponpas/${id}`, "DELETE")();
export const getBazaBenzins = fetchAPI('bazabenzins',"GET");
export const getBazaDizels = fetchAPI('bazadizels',"GET");
