import {
  getFuelItemFetch,
  addFuelFetch,
  getFuelsFetch,
  deleteFuelFetch,
  getBazaBenzins,
  getBazaDizels,
} from "../API";

//ACTION TYPES
const ADD_DATA = "ADD_DATA";
const SET_DATA = "SET_DATA";
const SET_DATA_PONPAS = "SET_DATA_PONPAS";
const DELETE_DATA = "DELETE_DATA";
const GET_BAZA_BENZINS = "GET_BAZA_BENZINS";
const GET_BAZA_DIZELS = "GET_BAZA_DIZELS";

export const MODULE_NAME = "fuels";
export const getPonpaItem = (state) => state[MODULE_NAME].ponpaItem;
export const getPonpasList = (state) => state[MODULE_NAME].ponpas;
export const getBazaBenzinsList = (state) => state[MODULE_NAME].bazaBenzins;
export const getBazaDizelsList = (state) => state[MODULE_NAME].bazaDizels;

const initialState = {
  ponpaItem: {},
  ponpas: [{}],
  bazaBenzins: [],
  bazaDizels: [],
};

export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_DATA:
      return {
        ...state,
        ponpaItem: { ...payload },
      };
    case ADD_DATA:
      return {
        ...state,
        ponpas: [...state.ponpas, payload],
        ponpaItem: { ...payload },
      };
    case SET_DATA_PONPAS:
      return {
        ...state,
        ponpas: payload,
      };
    case DELETE_DATA:
      return {
        ...state,
        ponpaItem: { ...state.ponpaItem },
        ponpas: state.ponpas.filter((ponpa) => ponpa.Id !== payload.Id),
      };
    case GET_BAZA_BENZINS:
      return {
        ...state,
        bazaBenzins: payload,
      };
    case GET_BAZA_DIZELS:
      return {
        ...state,
        bazaDizels: payload,
      };
    default:
      return state;
  }
}

export const setData = (payload) => ({ type: SET_DATA, payload });
export const addData = (payload) => ({ type: ADD_DATA, payload });
export const setDataPonpas = (payload) => ({ type: SET_DATA_PONPAS, payload });
export const deleteData = (payload) => ({ type: DELETE_DATA, payload });
export const getBazaBenzin = (payload) => ({ type: GET_BAZA_BENZINS, payload });
export const getBazaDizel = (payload) => ({ type: GET_BAZA_DIZELS, payload });

export const getDataFromAPI = () => async (dispatch) => {
  try {
    const data = await getFuelItemFetch();
    dispatch(setData(data));
  } catch (error) {
    console.log(error);
  }
};

export const addDataWithAPI = (ponpaItem) => async (dispatch) => {
  try {
    const response = await addFuelFetch(ponpaItem);
    dispatch(addData(response));
  } catch (error) {
    console.log(error);
  }
};

export const getDataTableFromAPI = () => async (dispatch) => {
  try {
    const response = await getFuelsFetch();
    dispatch(setDataPonpas(response));
  } catch (error) {
    console.log("GET DATA TABLE FROM API", error);
  }
};

export const deleteDataWithAPI = (id) => async (dispatch) => {
  try {
    const response = await deleteFuelFetch(id);
    dispatch(deleteData(response));
  } catch (error) {
    console.log("DELETE DATA WITH API", error);
  }
};

export const getBazaBenzinWithAPI = () => async (dispatch) => {
  try {
    const response = await getBazaBenzins();
    dispatch(getBazaBenzin(response));
  } catch (error) {
    console.log("GET BAZA BENZIN WITH API", error);
  }
};

export const getBazaDizelWithAPI = () => async (dispatch) => {
  try {
    const response = await getBazaDizels();
    dispatch(getBazaDizel(response));
  } catch (error) {
    console.log("GET BAZA BENZIN WITH API", error);
  }
};
