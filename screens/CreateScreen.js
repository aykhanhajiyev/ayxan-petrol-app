import React, { useState } from "react";
import {
  View,
  KeyboardAvoidingView,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
  ScrollView,
  Alert,
  Button,
} from "react-native";
import { connect } from "react-redux";

import COLORS from "../styles/colors";
import {
  CustomText,
  CustomInput,
  CustomButton,
  HomeCardItem,
  MenuIcon,
} from "../components";
import { addDataWithAPI, getPonpaItem } from "../store/fuels";

const mapStateToProps = (state) => ({
  ponpaSingleItem: getPonpaItem(state),
});

export const CreateScreen = connect(mapStateToProps, { addDataWithAPI })(
  ({ addDataWithAPI, navigation, ponpaSingleItem }) => {
    const [ponpaItem, setPonpaItem] = useState({
      Ponpa1: "",
      Ponpa2: "",
      Ponpa3: "",
      Ponpa4: "",
      Dizel: "",
    });

    const [isCalcMode, setIsCalcMode] = useState(false);
    const [differencePonpa, setDifferencePonpa] = useState([]);
    const [amounts, setAmounts] = useState({
      borrow: 0,
      difference: 0,
      sum: 0,
    });
    const resetFields = () => {
      setPonpaItem({
        Ponpa1: "",
        Ponpa2: "",
        Ponpa3: "",
        Ponpa4: "",
        Dizel: "",
      });
    };

    const addDataHandler = () => {
      addDataWithAPI({
        Ponpa1: ponpaItem.Ponpa1,
        Ponpa2: ponpaItem.Ponpa2,
        Ponpa3: ponpaItem.Ponpa3,
        Ponpa4: ponpaItem.Ponpa4,
        Dizel: ponpaItem.Dizel,
        DateTime: Date.now,
      });
      navigation.goBack();
    };

    const calculateHandler = () => {
      for (let ponpa in ponpaItem) {
        if (ponpaItem[ponpa].trim() === "") {
          Alert.alert(
            `Xəta ${ponpa}`,
            `Zəhmət olmasa ${ponpa} xanasını doldurun.`
          );
          return;
        }
      }
      setIsCalcMode(true);
      const differenceArr = [];
      differenceArr.push(
        `Ponpa 1*${ponpaItem.Ponpa1 - ponpaSingleItem.Ponpa1}`
      );
      differenceArr.push(
        `Ponpa 2*${ponpaItem.Ponpa2 - ponpaSingleItem.Ponpa2}`
      );
      differenceArr.push(
        `Ponpa 3*${ponpaItem.Ponpa3 - ponpaSingleItem.Ponpa3}`
      );
      differenceArr.push(
        `Ponpa 4*${ponpaItem.Ponpa4 - ponpaSingleItem.Ponpa4}`
      );
      differenceArr.push(`Dizel*${ponpaItem.Dizel - ponpaSingleItem.Dizel}`);

      setDifferencePonpa(differenceArr);

      let sum = 0;
      for (let i = 0; i < differenceArr.length; i++) {
        if (i !== differenceArr.length - 1) {
          sum += +differenceArr[i].split("*")[1] * 0.9;
        } else {
          sum += +differenceArr[i].split("*")[1] * 0.6;
        }
      }
      setAmounts((amounts) => ({
        ...amounts,
        sum,
      }));
    };

    const calculateDifferenceHandler = () => {
      setAmounts((amounts) => ({
        ...amounts,
        difference: amounts.sum - amounts.borrow,
      }));
    };
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
        style={styles.container}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          {isCalcMode ? (
            <KeyboardAvoidingView
              behavior={Platform.OS == "ios" ? "padding" : "height"}
            >
              <ScrollView>
                {differencePonpa.map((ponpa, index) => {
                  const header = ponpa.split("*")[0];
                  let text = "";
                  if (index === 4) {
                    text = `${ponpa.split("*")[1]}x0.6=${(
                      +ponpa.split("*")[1] * 0.6
                    ).toFixed(2)}`;
                  } else {
                    text = `${ponpa.split("*")[1]}x0.9=${(
                      +ponpa.split("*")[1] * 0.9
                    ).toFixed(2)}`;
                  }
                  return (
                    <HomeCardItem header={header} text={text} key={index} />
                  );
                })}

                <View style={styles.formRow}>
                  <CustomText size={18} style={{ paddingTop: 10 }}>
                    Borc:{" "}
                  </CustomText>
                  <CustomInput
                    style={{ width: Dimensions.get("window").width / 2 }}
                    keyboardType="numeric"
                    onChangeText={(borrow) =>
                      setAmounts((amounts) => ({
                        ...amounts,
                        borrow,
                      }))
                    }
                    value={amounts.borrow.toString()}
                  />
                  <MenuIcon
                    name={"md-calculator"}
                    size={28}
                    color={COLORS.header}
                    style={{ marginTop: 10 }}
                    onPress={calculateDifferenceHandler}
                  />
                </View>
                <View style={[styles.formRow, { paddingVertical: 10 }]}>
                  <CustomText weight="semibold" size={18}>
                    Toplam məbləğ:
                  </CustomText>
                  <CustomText size={18}>
                    {amounts.sum.toFixed(2)} AZN
                  </CustomText>
                </View>
                <View style={[styles.formRow, { paddingVertical: 10 }]}>
                  <CustomText weight="semibold" size={18}>
                    Qalıq məbləğ:
                  </CustomText>
                  <CustomText size={18}>
                    {amounts.difference.toFixed(2)} AZN
                  </CustomText>
                </View>
                <View style={styles.formRow}>
                  <CustomButton
                    title="Geri qayıt"
                    style={{ width: Dimensions.get("window").width / 2 - 40 }}
                    onPress={() => setIsCalcMode(false)}
                  />
                  <CustomButton
                    title="Əlavə et"
                    style={{
                      width: Dimensions.get("window").width / 2 - 40,
                      backgroundColor: "#8BC34A",
                    }}
                    onPress={addDataHandler}
                  />
                </View>
              </ScrollView>
            </KeyboardAvoidingView>
          ) : (
            <ScrollView style={styles.formContainer}>
              <View style={styles.formRow}>
                <CustomText size={18}>Ponpa 1</CustomText>
                <CustomInput
                  style={{
                    width: "70%",
                    marginBottom: 15,
                  }}
                  keyboardType="number-pad"
                  onChangeText={(text) =>
                    setPonpaItem((items) => ({
                      ...items,
                      Ponpa1: text,
                    }))
                  }
                  value={ponpaItem.Ponpa1}
                />
              </View>
              <View style={styles.formRow}>
                <CustomText size={18}>Ponpa 2</CustomText>
                <CustomInput
                  style={{
                    width: "70%",
                    marginBottom: 15,
                  }}
                  keyboardType="number-pad"
                  onChangeText={(text) =>
                    setPonpaItem((items) => ({
                      ...items,
                      Ponpa2: text,
                    }))
                  }
                  value={ponpaItem.Ponpa2}
                />
              </View>
              <View style={styles.formRow}>
                <CustomText size={18}>Ponpa 3</CustomText>
                <CustomInput
                  style={{
                    width: "70%",
                    marginBottom: 15,
                  }}
                  keyboardType="number-pad"
                  onChangeText={(text) =>
                    setPonpaItem((items) => ({
                      ...items,
                      Ponpa3: text,
                    }))
                  }
                  value={ponpaItem.Ponpa3}
                />
              </View>
              <View style={styles.formRow}>
                <CustomText size={18}>Ponpa 4</CustomText>
                <CustomInput
                  style={{
                    width: "70%",
                    marginBottom: 15,
                  }}
                  keyboardType="number-pad"
                  onChangeText={(text) =>
                    setPonpaItem((items) => ({
                      ...items,
                      Ponpa4: text,
                    }))
                  }
                  value={ponpaItem.Ponpa4}
                />
              </View>
              <View style={styles.formRow}>
                <CustomText size={18}>Dizel</CustomText>
                <CustomInput
                  style={{
                    width: "70%",
                    marginBottom: 15,
                  }}
                  keyboardType="number-pad"
                  onChangeText={(text) =>
                    setPonpaItem((items) => ({
                      ...items,
                      Dizel: text,
                    }))
                  }
                  value={ponpaItem.Dizel}
                />
              </View>
              <View style={styles.formRow}>
                <CustomButton
                  title="Sıfırla"
                  style={{ width: Dimensions.get("window").width / 2 - 40 }}
                  onPress={resetFields}
                />
                <CustomButton
                  title="Hesabla"
                  style={{
                    width: Dimensions.get("window").width / 2 - 40,
                    backgroundColor: "#8BC34A",
                  }}
                  onPress={calculateHandler}
                />
              </View>
            </ScrollView>
          )}
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  formRow: {
    flexDirection: "row",
    alignItems: "center",
    width: Dimensions.get("screen").width - 50,
    justifyContent: "space-between",
  },
});
