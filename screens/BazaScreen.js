import React, { useEffect } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";

import { CustomText, CustomButton } from "../components";
import COLORS from "../styles/colors";
import {
  getBazaBenzinsList,
  getBazaBenzinWithAPI,
  getBazaDizelsList,
  getBazaDizelWithAPI,
} from "../store/fuels";

const mapStateToProps = (state) => ({
  bazaBenzins: getBazaBenzinsList(state),
  bazaDizels: getBazaDizelsList(state),
});

export const BazaScreen = connect(mapStateToProps, {
  getBazaBenzinWithAPI,
  getBazaDizelWithAPI,
})(({ bazaBenzins, bazaDizels, getBazaBenzinWithAPI, getBazaDizelWithAPI }) => {
  useEffect(() => {
    getBazaBenzinWithAPI();
    getBazaDizelWithAPI();
  }, []);
  let sumOfBenzin = 0;
  let sumOfDizel = 0;
  for (let benzin of bazaBenzins) {
    sumOfBenzin += benzin.Benzin;
  }
  for (let dizel of bazaDizels) {
    sumOfDizel += dizel.Dizel;
  }
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.card}>
        <View style={styles.cardRow}>
          <CustomText weight="semibold" size={16} style={styles.cardTitle}>
            Benzin qalığı:
          </CustomText>
          <CustomText weight="semibold" size={16} style={styles.cardTitle}>
            {sumOfBenzin} litr
          </CustomText>
        </View>
        <View style={styles.buttonWrapper}>
          <CustomButton title="Əlavə et" style={styles.button} />
        </View>
      </View>
      <View style={styles.card}>
        <View style={styles.cardRow}>
          <CustomText weight="semibold" size={16} style={styles.cardTitle}>
            Dizel qalığı:
          </CustomText>
          <CustomText weight="semibold" size={16} style={styles.cardTitle}>
            {sumOfDizel} litr
          </CustomText>
        </View>
        <View style={styles.buttonWrapper}>
          <CustomButton title="Əlavə et" />
        </View>
      </View>
    </ScrollView>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 15,
  },
  card: {
    width: "100%",
    backgroundColor: "#eee",
    borderWidth: 1,
    borderColor: COLORS.main,
    borderRadius: 20,
    paddingTop: 20,
    paddingHorizontal: 10,
    marginBottom: 30,
  },
  cardRow: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cardTitle: {
    color: "rgba(0,0,0,0.8)",
  },
  buttonWrapper: {
    alignItems: "center",
    marginTop: 5,
  },
  button: {
    backgroundColor: COLORS.header,
  },
});
