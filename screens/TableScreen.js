import React, { useState, useEffect } from "react";
import {
  ScrollView,
  View,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from "react-native";
import { Table, Row } from "react-native-table-component";
import { connect } from "react-redux";

import {
  getDataTableFromAPI,
  getPonpasList,
  deleteDataWithAPI,
} from "../store/fuels";
import COLORS from "../styles/colors";


const mapStateToProps = (state) => ({
  ponpaList: getPonpasList(state),
});

export const TableScreen = connect(mapStateToProps, {
  getDataTableFromAPI,
  deleteDataWithAPI,
})(({ ponpaList, getDataTableFromAPI, deleteDataWithAPI }) => {
  const [tableHead, setTableHead] = useState([
    "Id",
    "Ponpa1",
    "Ponpa2",
    "Ponpa3",
    "Ponpa4",
    "Dizel",
    "Tarix",
  ]);
  const [widthArr, setWidhtArr] = useState([80, 80, 80, 80, 80, 80]);
  const tableData = [];
  useEffect(() => {
    getDataTableFromAPI();
  }, []);
  for (let ponpa in ponpaList.reverse()) {
    const tableRow = [];
    tableRow.push(ponpaList[ponpa].Id);
    tableRow.push(ponpaList[ponpa].Ponpa1);
    tableRow.push(ponpaList[ponpa].Ponpa2);
    tableRow.push(ponpaList[ponpa].Ponpa3);
    tableRow.push(ponpaList[ponpa].Ponpa4);
    tableRow.push(ponpaList[ponpa].Dizel);
    const splitted_date = `${
      ponpaList[ponpa]?.DateTime?.split("-")[2].split("T")[0]
    }-${ponpaList[ponpa]?.DateTime?.split("-")[1]}-${
      ponpaList[ponpa]?.DateTime?.split("-")[0]
    }`;
    tableRow.push(splitted_date);
    tableData.push(tableRow);
  }
  const deleteHandler = (id) => {
    Alert.alert(
      "Silmək istədiyinizdən əminsiniz?",
      "Silinən məlumatları geri qaytarmaq mümkün olmayacaq.",
      [
        {
          text: "Ləğv et",
          style: "destructive",
        },
        {
          text: "Sil",
          onPress: () => deleteDataWithAPI(id),
        },
      ]
    );
  };
  return (
    <View style={styles.container}>
      <ScrollView horizontal={true}>
        <View>
          <Table borderStyle={{ borderColor: "#C1C0B9" }}>
            <Row
              data={tableHead}
              widthArr={widthArr}
              style={styles.header}
              textStyle={styles.text}
            />
          </Table>
          <ScrollView style={styles.dataWrapper}>
            <Table borderStyle={{ borderColor: "#C1C0B9" }}>
              {tableData.map((rowData,index) => (
                <TouchableOpacity
                  key={index}
                  onLongPress={() => deleteHandler(rowData[0])}
                >
                  <Row
                    key={index}
                    data={rowData}
                    widthArr={widthArr}
                    style={styles.row}
                    textStyle={styles.textRow}
                  />
                </TouchableOpacity>
              ))}
            </Table>
          </ScrollView>
        </View>
      </ScrollView>
    </View>
  );
});
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header: {
    height: 50,
    backgroundColor: COLORS.header,
  },
  text: {
    textAlign: "center",
    color: "white",
    fontFamily: "MontserratBold",
  },
  textRow: {
    fontFamily: "MontserratRegular",
    textAlign: "center",
  },
  row: {
    height: 40,
  },
});
