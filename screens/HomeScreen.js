import React, { useEffect } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";

import { CustomText, HomeCardItem } from "../components";
import COLORS from "../styles/colors";
import { getPonpaItem, getDataFromAPI } from "../store/fuels";

const mapStateToProps = (state) => ({
  ponpaItem: getPonpaItem(state),
});
export const HomeScreen = connect(mapStateToProps, { getDataFromAPI })(
  ({ ponpaItem, getDataFromAPI, navigation }) => {
    useEffect(() => {
      getDataFromAPI(); 
    }, []);
    const MONTHS = [
      "Yanvar",
      "Fevral",
      "Mart",
      "Aprel",
      "May",
      "Iyun",
      "Iyul",
      "Avqust",
      "Sentyabr",
      "Oktyabr",
      "Noyabr",
      "Dekabr",
    ];
    return (
      <ScrollView style={styles.container}>
        <HomeCardItem header={"Ponpa 1:"} text={ponpaItem?.Ponpa1} key={1} />
        <HomeCardItem header={"Ponpa 2:"} text={ponpaItem?.Ponpa2} key={2} />
        <HomeCardItem header={"Ponpa 3:"} text={ponpaItem?.Ponpa3} key={3} />
        <HomeCardItem header={"Ponpa 4:"} text={ponpaItem?.Ponpa4} key={4} />
        <HomeCardItem header={"Dizel:"} text={ponpaItem?.Dizel} key={5} />
        <View style={styles.cardDate}>
          <CustomText>
            Məlumat yenilənib:{" "}
            {ponpaItem?.DateTime?.split("-")[2].split("T")[0]}-
            {MONTHS[+ponpaItem?.DateTime?.split("-")[1] - 1]}-
            {ponpaItem?.DateTime?.split("-")[0]}
          </CustomText>
        </View>
      </ScrollView>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: COLORS.white,
  },
  cardDate: {
    alignItems: "center",
    backgroundColor: "#FFEB3B",
    padding: 10,
    borderRadius: 20,
    marginVertical: 10,
  },
});
