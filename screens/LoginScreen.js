import React, { useEffect } from "react";
import {
  View,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
  Dimensions,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import { connect } from "react-redux";

import COLORS from "../styles/colors";
import Logo from "../assets/APlogo.png";
import { CustomButton,CustomText, CustomInput } from "../components";

import { getDataFromAPI } from "../store/fuels";

export const LoginScreen = connect(null,{getDataFromAPI})(({navigation,getDataFromAPI}) => {
  // useEffect(()=>{
  //   getDataFromAPI();
  // },[])
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.formContainer}>
          <Image source={Logo} style={styles.img} resizeMode="contain" />
          <CustomText size={18} weight="semibold">
            İstifadəçi adı
          </CustomText>
          <CustomInput
            style={{
              width: Dimensions.get("window").width - 100,
              marginBottom: 15,
            }}
          />
          <CustomText size={18} weight="semibold">
            Şifrə
          </CustomText>
          <CustomInput
            style={{ width: Dimensions.get("window").width - 100 }}
            secureTextEntry={true}
          />
          <CustomButton title="Daxil ol" onPress={() => navigation.navigate("Home")} />
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
    justifyContent: "center",
  },
  formContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  img: {
    width: Dimensions.get("window").width - 150,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
});
